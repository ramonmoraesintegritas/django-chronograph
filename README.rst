===========
Chronograph
===========

Chronograph is a simple application that allows you to control the frequency at
which a Django management command gets run.

To help explain how this is useful, let's consider a simple example.  Say you've
written an application that displays weather on your site.  You've written a
custom management command that you can execute which updates the weather::

    python manage.py update_weather

You would like to be able to run this command every hour so that you always have
the latest weather information displayed on your site.  Rather than having to
edit your crontab, ``cronograph`` allows you to simply add this functionality
via your admin.

Installing Chronograph
======================

Installing ``chronograph`` is pretty simple.  First add it into ``INSTALLED_APPS``
in your ``settings.py`` file.  If you're running a version of Django older than
revision 9739 (basically anything after 1.0 but before 1.1), then you'll need to
add the following to your project's ``urls.py``::

	url(r'^admin/chronograph/job/(?P<pk>\d+)/run/$',
		'chronograph.views.job_run', name="admin_chronograph_job_run")


After this run `syncdb``.  The only thing left to do is set up a periodic call to
run the jobs.

If you're using `cron`, the following example can be added to your `crontab`::

    * * * * * /path/to/your/env/bin/python /path/to/your/project/manage.py cron

You're done!  Every minute ``cron`` will check to see if you have any pending jobs
and if you do they'll be run.  No more mucking about with your ``crontab``.

If you have a more complicated setup where ``manange.py`` might not work by default
see the section below on installing ``chronograph`` in a virtual environment.

Using Chronograph
=================

If you've completed the above steps, you're all done.  Now you can add some jobs to the system.
Remember, ``chronograph`` is designed to run any install ``django-admin`` management command and
it accomodates command-line arguments as well.


