from datetime import timedelta
from celery.decorators import periodic_task
from chronograph import services


MINUTE = timedelta(minutes=1)


@periodic_task(name='chronograph_running_due_jobs', run_every=MINUTE)
def running_due_jobs():
    services.running_due_jobs()
