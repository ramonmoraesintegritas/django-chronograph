import logging
from django.core.management.base import BaseCommand
from chronograph import services


logger = logging.getLogger('chronograph')


class Command(BaseCommand):

    help = 'Runs all jobs that are due.'

    def handle(self, *args, **options):
        services.running_due_jobs()
