import logging
from chronograph.models import Job


logger = logging.getLogger('chronograph')


def running_due_jobs():
    jobs = Job.objects.due()
    logger.info('found {} job on due mode'.format(len(jobs)))
    for job in jobs:
        logger.info('running [{}]'.format(job.name))
        job.run()
