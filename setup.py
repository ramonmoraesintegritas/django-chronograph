from setuptools import setup


setup(
    name='django-chronograph',
    version='svn-r21.6',
    description='Django chronograph application.',
    author='Weston Nielson',
    author_email='wnielson@gmail.com',
    packages = [
        'chronograph',
    ],
    install_requires=[
        'python-dateutil==1.5',
        'django-celery==2.3.3',
    ],
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Environment :: Web Environment',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Programming Language :: Python',
        'Framework :: Django',
    ],
)
